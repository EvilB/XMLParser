import XCTest
@testable import XMLParserTests

XCTMain([
    testCase(XMLParserTests.allTests),
])
